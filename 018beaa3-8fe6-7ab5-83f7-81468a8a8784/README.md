## test stand Hydropulser

<div align="right">

### IRI: [`https://w3id.org/fst/resource/018beaa3-8fe6-7ab5-83f7-81468a8a8784`](https://w3id.org/fst/resource/018beaa3-8fe6-7ab5-83f7-81468a8a8784)
### UUID: `018beaa3-8fe6-7ab5-83f7-81468a8a8784`
### identifier: `350.5`

</div>

## Keywords: dynamic-forces, hardware-in-the-loop, heterogeneous-test-setups, modular, real-time-simulation, servo-hydraulic, temperature-controllable, temperature-controllable-environment, temperature-controllable-test-environment, temperature-controlled, test-rig, test-stand, uni-axial, vertical

<img width="1200" src=/018beaa3-8fe6-7ab5-83f7-81468a8a8784/IMG/IMG_hydropulser.jpg>

## General Info

| property | value |
|-:|:-|
| comment: |  |
| manufacturer: | MTS |
| name: | Hydropulser |
| serial number: | 100191685 |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Rexer |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject |

