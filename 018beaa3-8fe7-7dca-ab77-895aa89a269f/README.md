## test stand Sirupmischanlage

<div align="right">

### IRI: [`https://w3id.org/fst/resource/018beaa3-8fe7-7dca-ab77-895aa89a269f`](https://w3id.org/fst/resource/018beaa3-8fe7-7dca-ab77-895aa89a269f)
### UUID: `018beaa3-8fe7-7dca-ab77-895aa89a269f`

</div>

## Keywords: hardware-in-the-loop, real-time-simulation, test-rig, test-stand

<img width="1200" src=/018beaa3-8fe7-7dca-ab77-895aa89a269f/IMG/IMG_sirupmischanlage.jpg>

## General Info

| property | value |
|-:|:-|
| comment: |  |
| manufacturer: | FST |
| name: | Sirupmischanlage |
| serial number: | None |
|-|-|
| owner: | FST |
| maintainer: | Wetterich |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject |

