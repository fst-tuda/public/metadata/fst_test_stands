## Resilienzdemonstrator

<div align="right">

### IRI: [`https://w3id.org/fst/resource/0192fd1d-3b09-734b-b4e3-621fc590d00c`](https://w3id.org/fst/resource/0192fd1d-3b09-734b-b4e3-621fc590d00c)
### UUID: `0192fd1d-3b09-734b-b4e3-621fc590d00c`

</div>

## Keywords: adaptivity, distributed control, resilience, test-rig, test-stand, time series analysis, urban water distribution systems

<img width="1200" src=/0192fd1d-3b09-734b-b4e3-621fc590d00c/IMG/image_241018_Resilienzdemonstrator_Foto_Logan.jpg>

## General Info

| property | value |
|-:|:-|
| comment: |  |
| manufacturer: | FST |
| name: | Resilienzdemonstrator |
| serial number: | None |
| used procedure: | None |
|-|-|
| owner: | FST |
| maintainer: | Logan |
| last known location: | None |
| last modification: | None |
|-|-|
| related resources: | None |

<br clear="right"/>

## Additional Info

&#160;

| property | value |
|-:|:-|
| types: | http://purl.org/dc/dcmitype/PhysicalObject |

